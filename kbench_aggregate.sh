#/bin/bash
echo "" > results.csv


for dir in ~/kbench/k-bench/exjobb/*
do 
    result=`cd $dir/dp_sysbench_density; bash aggregate_results.sh | grep "Aggregate throughput" | awk {'print $4'}`
    
    echo "${result}" >> results.csv
done
