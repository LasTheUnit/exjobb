#/bin/bash
Num_Instances=5;

Agg_throughput=0;
for((num=0;num < ${Num_Instances};num++))
{ 
	throughput=`cat */kbench-pod-oid-0-tid-${num}/sysbench.out | grep "events per second" | awk {'print $4'}`
	echo "Throughput of pod $num is $throughput";
	Agg_throughput=`echo "$throughput + $Agg_throughput" | bc`
}
echo "Aggregate throughput = $Agg_throughput";
