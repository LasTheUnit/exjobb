#/bin/sh

echo "auto enp0s5
iface enp0s5  inet static
 address 10.8.12.110/24
 gateway 10.8.12.1
 dns-nameservers 1.1.1.1 8.8.8.8" >> /etc/network/interfaces
